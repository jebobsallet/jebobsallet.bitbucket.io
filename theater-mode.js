var ignoreResize;
var inTheaterMode;
var blackBg;
var playerStyle;
var chatStyle;

function updateTheaterMode(player, chat) {
    if (!blackBg) {
        blackBg = document.createElement("div");
        blackBg.style.position = "fixed";
        blackBg.style.top = 0;
        blackBg.style.left = 0;
        blackBg.style.width = "100%";
        blackBg.style.height = "100%";
        blackBg.style.zIndex = 999999998;
        blackBg.style.background = "black";
    }
    if (blackBg.parentElement != document.body) {
        document.body.appendChild(blackBg);
    }

    player.style.position ="fixed";
    player.style.width = "calc(100vw - 340px)";
    player.style.height = "auto";
    player.style.left = "0";
    player.style.top = "50%";
    player.style.transform = "translate(0, -50%)";
    player.style.zIndex = 999999999;

    var rect = player.getBoundingClientRect();
    document.getElementById("ipopp").style.height
        = Math.min(Math.floor(rect.width * parseFloat(9) / 16),
            blackBg.getBoundingClientRect().height) + "px";

    chat.style.position = "fixed";
    chat.style.width = "340px";
    chat.style.right = 0;
    chat.style.top = 0;
    chat.style.zIndex = 999999999;
}

function onTheaterMode(e) {
    var player = document.getElementById("player-section");
    var chat = document.getElementById("bchat0");

    // Entering theater mode
    if (!inTheaterMode) {
        playerStyle = Object.assign({}, player.style);
        chatStyle = Object.assign({}, chat.style);

        updateTheaterMode(player, chat);

        document.documentElement.style.overflow = "hidden";
        document.documentElement.style.height = "100%";
        document.body.style.overflow = "auto";
        document.body.style.height = "100%";

    // Exiting theater mode
    } else {
        var iframe = document.getElementById("ipopp");
        iframe.style.height = "530px";
        iframe.style.width = "100%";

        document.documentElement.style.overflow = "visible";
        document.documentElement.style.height = "auto";
        document.body.style.overflow = "visible";
        document.body.style.height = "auto";

        player.style = playerStyle;
        chat.style = chatStyle;

        document.body.removeChild(blackBg);
    }
    inTheaterMode = !inTheaterMode;
}


window.addEventListener('message', function(e) {
    // Origin checking? Do you even need it for this application?
    //if (e.origin == "https://jebobsallet.bitbucket.io") {
        if (e.data.event_id == 'CustomEvent') {
            onTheaterMode(e.data);
        } else if (e.data.event_id == 'CustomEvent2') {
            ignoreResize = e.data.data;
        }
    //}
});

window.addEventListener('resize', function(e) {
    if (!ignoreResize && inTheaterMode) {
        var player = document.getElementById("player-section");
        var chat = document.getElementById("bchat0");

        updateTheaterMode(player, chat);
    }
});